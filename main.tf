terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
  client_id = var.client_id
  client_secret = var.client_secret
  features {}
}

resource "azurerm_resource_group" "ejbest" {
  name     = "example-resources"
  location = "West Europe"
}

resource "azurerm_dns_zone" "ejbest" {
  name                = "waterskiingguy.com"
  resource_group_name = azurerm_resource_group.ejbest.name
}

resource "azurerm_dns_cname_record" "target" {
  name                = "target"
  zone_name           = azurerm_dns_zone.ejbest.name
  resource_group_name = azurerm_resource_group.ejbest.name
  ttl                 = 300
  record              = "waterskiingguy.com"
}

resource "azurerm_dns_cname_record" "ejbest" {
  name                = "ejbestTest"
  zone_name           = azurerm_dns_zone.ejbest.name
  resource_group_name = azurerm_resource_group.ejbest.name
  ttl                 = 300
  target_resource_id  = azurerm_dns_cname_record.target.id
}
